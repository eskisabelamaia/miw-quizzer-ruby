require 'sinatra'
require 'slim'
 
require_relative 'quizzer/evaluator.rb'
require_relative 'quizzer/filemanagement.rb'
require_relative 'quizzer/deserialization.rb'
require_relative 'quizzer/quiz.rb'
require_relative 'quizzer/assessment.rb'

get '/' do
  slim :index
end

post '/' do

	quizz = params[:quizz].to_s
	assessment = params[:assessment].to_s
	calculatedScores = 'Incorrect input'

	unless (quizz.nil? || assessment.nil?)
	    begin
	    	fileManagement = FileManagement.new()
			evaluator = Evaluator.new()
			deserialization = Deserialization.new()
			quizzJson, assessmentJson = fileManagement.parse_json_files(quizz, assessment)
			quizzObj = Quiz.new(deserialization.get_questions(quizzJson))
			assessmentObj = Assessment.new(deserialization.get_items(assessmentJson))
			calculatedScores = evaluator.evaluate(quizzObj, assessmentObj)
			scoreJson = calculatedScores.json()
	    end
	end

	slim :scores, :locals => {:scores => scoreJson}

end