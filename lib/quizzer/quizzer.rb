require_relative 'Evaluator'
require_relative 'filemanagement'
require_relative 'options'
require_relative 'deserialization'
require_relative 'quiz'
require_relative 'assessment'
require 'json'

class Main
	def initialize(argv)
		@fileManagement = FileManagement.new()
		@evaluator = Evaluator.new()
		@deserialization = Deserialization.new()
		@options = Options.new(argv)
	end
        
	def run()
		if @options.fileQuizz and @options.fileAssessment
			quizz, assessment = @fileManagement.read_files(@options.fileQuizz, @options.fileAssessment)
			quizzJson, assessmentJson = @fileManagement.parse_json_files(quizz, assessment)
			quizzObj = Quiz.new(@deserialization.get_questions(quizzJson))
			assessmentObj = Assessment.new(@deserialization.get_items(assessmentJson))
			calculatedScores = @evaluator.evaluate(quizzObj, assessmentObj)
			calculatedScores.create_file()
			puts "Calculated scores have been written in the directory bin/Scores.json"
		elsif @options.fileManifest
            counter = 1
			manifestObj = @fileManagement.read_file(@options.fileManifest)
			manifest = @fileManagement.parse_json_file(manifestObj)
			manifest["tests"].each { |item|
				puts "Scores #{counter}"
				quizzJson = @fileManagement.parse_url_json_file(item["quizz"])
				assessmentJson = @fileManagement.parse_url_json_file(item["assessment"])
				quizObj = Quiz.new(@deserialization.get_questions(quizzJson))
				assessmentObj = Assessment.new(@deserialization.get_items(assessmentJson))
				calculatedScores = @evaluator.evaluate(quizObj, assessmentObj)
      			scoresJson = @fileManagement.parse_url_json_file(item["scores"])
      			scoresObj = Scores.new(@deserialization.get_scores(scoresJson))
				calculatedScores.compareScores(scoresObj)
				counter +=1
			}
		else
			raise "Try again"
		end
    end
end