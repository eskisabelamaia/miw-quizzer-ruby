
class Scores

	attr_accessor :scores

	def initialize(scores)
		@scores = scores
	end
  
	def create_file()
		scoreJson = "{\"scores\":["
		@scores.length.times do |index|
			scoreJson += "{\"studentId\": " + @scores[index].studentId.to_s + ", \"value\": " + @scores[index].value.to_s + "}"
			if index != @scores.length-1 then scoreJson += ", " end
		end
		scoreJson += "]}"
		File.write('scores.json',scoreJson)
	end
  
  	def compareScores (scoresJson)
		counter = 0
		counterstudent = 0
		@scores.each { |score|
			counterstudent +=1
			scoresJson.scores.each { |json|
				if (json.studentId == score.studentId && json.value == score.value) then
					counter +=1
				end
			}
		}
		if counter == counterstudent then
			puts "Calculated scores are equal"
			# Return for test
			return true
		else
			# Return for test
			puts "Calculated scores are not equal"
			return false
		end
    end

    def json()
		scoreJson = "{\"scores\":["
		@scores.length.times do |index|
			scoreJson += "{\"studentId\": " + @scores[index].studentId.to_s + ", \"value\": " + @scores[index].value.to_s + "}"
			if index != @scores.length-1 then scoreJson += ", " end
		end
		scoreJson += "]}"
		return scoreJson
	end
end