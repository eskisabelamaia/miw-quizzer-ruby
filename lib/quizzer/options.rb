require 'optparse'

class Options
        
    attr_reader :fileQuizz
	attr_reader :fileAssessment
	attr_reader :fileManifest
		
     def initialize(argv)
		@fileQuizz
		@fileAssessment
		@fileManifest
        parse(argv)
    end
        
	def parse(argv)
		OptionParser.new do |opts|
			opts.banner = "Usage: ruby quizzer [ options ]"
			opts.on("-f", "--files Quizz.json,Assessment.json", Array, "JSON files") do |files|
			@fileQuizz = files[0]
			@fileAssessment = files[1]
		end

		opts.on("-m", "--manifest Manifest.json", String, "Manifest file") do |p|
			@fileManifest = p
		end
		
		opts.on("-h", "--help", "Show this message") do
			puts opts
			exit
		end

		begin
			opts.parse!(argv)
			rescue OptionParser::ParseError => e
				STDERR.puts e.message, "\n", opts
					exit(-1)
				end
			end
		end 
    end