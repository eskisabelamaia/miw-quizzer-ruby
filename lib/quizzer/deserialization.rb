require_relative 'alternative'
require_relative 'multichoice'
require_relative 'truefalse'
require_relative 'answer'
require_relative 'item'

class Deserialization

	def get_questions(json)
		lstQuestions = []
		json["questions"].each { |question|
			readQuestion = nil
			case question["type"]
			when "multichoice"
				alternatives = get_alternatives(question["alternatives"])
				readQuestion = Multichoice.new(question["id"], question["questionText"], alternatives)
			when "truefalse"
				readQuestion = TrueFalse.new(question["id"], question["questionText"], question["correct"], question["valueOK"],question["valueFailed"], question["feedback"])
			else
				puts "Invalid question type" 
			end
			lstQuestions << readQuestion
		}
		lstQuestions
	end
	
	def get_alternatives(alternatives)
		lstAlternatives = []
		alternatives.each { |alternative|
			readAlternative = Alternative.new(alternative["text"], alternative["code"], alternative["value"])
			lstAlternatives << readAlternative
		}
		lstAlternatives
	end

	def get_answers(answers)
		lstAnswers = []
		answers.each { |answer|
			readAnswer = Answer.new(answer["question"], answer["value"])
			lstAnswers << readAnswer
		}
		lstAnswers
	end

	def get_items(assessmentJson)
		lstItems = []
		assessmentJson["items"].each { |item|
		  answers = get_answers(item["answers"])
		  itemObj = Item.new(item["studentId"], answers)
		  lstItems << itemObj
		}
		lstItems
	end

	def get_scores(scoresJson)
		lstScores = []
		scoresJson["scores"].each { |score|
			readScore = Score.new(score["studentId"], score["value"])
			lstScores << readScore
		}
		lstScores
	end
end