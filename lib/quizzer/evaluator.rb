require_relative 'scores'
require_relative 'score'
require_relative 'item'

class Evaluator
	
	def evaluate(quizObj, assessmentObj)
		lstScores = []
		assessmentObj.items.each { |item|
			studentId = item.studentId
			value = grade_calculation(quizObj,studentId, item.answers)
			scoreObj = Score.new(studentId, value)
			lstScores.push(scoreObj)
		}
		scoresObj = Scores.new(lstScores)
		scoresObj
	end

	def grade_calculation(quizObj, studentId, studentAnswers)
		grade = 0.0
		studentAnswers.each { |item|
			question = find_student_answer(item.question, quizObj)
			if question.class == Multichoice
				grade += grade_multichoice(question.alternatives, item.value)
			elsif question.class == TrueFalse
				grade += grade_truefalse(question, item.value)
			else
				puts "Invalid question type"
			end
		}
		grade
	end

	def find_student_answer(code, quizObj)
		elements = quizObj.questions.select { |item| item.id == code}
		elements[0]
	end

	def grade_multichoice(alternatives, value)
		elements = alternatives.select{ |item| item.code == value }
		elements[0].value
	end

	def grade_truefalse(question, value)
		if question.correct == value
			question.valueOK
		else
			question.valueFailed
		end
	end
end