require "net/http"
require "json"

class FileManagement

	def read_file(filename)
		strFile = File.read(filename)
	end
	
	def read_files(fileQuizz, fileAssessment)
		strQuizz = File.read(fileQuizz)
		strAssessment = File.read(fileAssessment)
		return strQuizz, strAssessment
	end

	def parse_json_files (quizzFile, assessmentFile)
		quizzJson = JSON.parse(quizzFile)
		assessmentJson = JSON.parse(assessmentFile)
		return quizzJson, assessmentJson
	end
	
	def parse_json_file (file)
		fileJson = JSON.parse(file)
		return fileJson
	end
	
	def parse_url_json_files (quizzUrl, assessmentUrl)
        quizzRsp = Net::HTTP.get_response(URI.parse(quizzUrl))
		assessmentRsp = Net::HTTP.get_response(URI.parse(assessmentUrl))
        quizzJson = JSON.parse(quizzRsp.body)
		assessmentJson = JSON.parse(assessmentRsp.body)
        return quizzJson, assessmentJson
    end
	
	def parse_url_json_file (fileUrl)
        rsp = Net::HTTP.get_response(URI.parse(fileUrl))
        json = JSON.parse(rsp.body)
        return json
    end
end